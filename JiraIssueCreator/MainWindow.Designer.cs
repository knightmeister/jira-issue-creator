﻿namespace JiraIssueCreator
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLoggedInAs = new System.Windows.Forms.Label();
            this.lnkUsername = new System.Windows.Forms.LinkLabel();
            this.lblProject = new System.Windows.Forms.Label();
            this.lblIssueType = new System.Windows.Forms.Label();
            this.lblAssignee = new System.Windows.Forms.Label();
            this.ddlProjects = new System.Windows.Forms.ComboBox();
            this.ddlIssueType = new System.Windows.Forms.ComboBox();
            this.ddlAssignee = new System.Windows.Forms.ComboBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtSummary = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.lblComponents = new System.Windows.Forms.Label();
            this.componentsPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.ddlPriority = new System.Windows.Forms.ComboBox();
            this.lblPriority = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lnkIssueCreated = new System.Windows.Forms.LinkLabel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblLoggedInAs
            // 
            this.lblLoggedInAs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLoggedInAs.AutoSize = true;
            this.lblLoggedInAs.Location = new System.Drawing.Point(12, 472);
            this.lblLoggedInAs.Name = "lblLoggedInAs";
            this.lblLoggedInAs.Size = new System.Drawing.Size(68, 13);
            this.lblLoggedInAs.TabIndex = 3;
            this.lblLoggedInAs.Text = "Logged in as";
            // 
            // lnkUsername
            // 
            this.lnkUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lnkUsername.AutoSize = true;
            this.lnkUsername.Location = new System.Drawing.Point(86, 472);
            this.lnkUsername.Name = "lnkUsername";
            this.lnkUsername.Size = new System.Drawing.Size(55, 13);
            this.lnkUsername.TabIndex = 4;
            this.lnkUsername.TabStop = true;
            this.lnkUsername.Text = "linkLabel1";
            // 
            // lblProject
            // 
            this.lblProject.AutoSize = true;
            this.lblProject.Location = new System.Drawing.Point(12, 42);
            this.lblProject.Name = "lblProject";
            this.lblProject.Size = new System.Drawing.Size(43, 13);
            this.lblProject.TabIndex = 5;
            this.lblProject.Text = "Project:";
            // 
            // lblIssueType
            // 
            this.lblIssueType.AutoSize = true;
            this.lblIssueType.Location = new System.Drawing.Point(12, 69);
            this.lblIssueType.Name = "lblIssueType";
            this.lblIssueType.Size = new System.Drawing.Size(58, 13);
            this.lblIssueType.TabIndex = 6;
            this.lblIssueType.Text = "Issue type:";
            // 
            // lblAssignee
            // 
            this.lblAssignee.AutoSize = true;
            this.lblAssignee.Location = new System.Drawing.Point(12, 96);
            this.lblAssignee.Name = "lblAssignee";
            this.lblAssignee.Size = new System.Drawing.Size(53, 13);
            this.lblAssignee.TabIndex = 7;
            this.lblAssignee.Text = "Assignee:";
            // 
            // ddlProjects
            // 
            this.ddlProjects.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddlProjects.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlProjects.FormattingEnabled = true;
            this.ddlProjects.Location = new System.Drawing.Point(89, 39);
            this.ddlProjects.Name = "ddlProjects";
            this.ddlProjects.Size = new System.Drawing.Size(529, 21);
            this.ddlProjects.TabIndex = 8;
            this.ddlProjects.SelectedValueChanged += new System.EventHandler(this.ddlProjects_SelectedValueChanged);
            // 
            // ddlIssueType
            // 
            this.ddlIssueType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ddlIssueType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlIssueType.FormattingEnabled = true;
            this.ddlIssueType.Location = new System.Drawing.Point(89, 66);
            this.ddlIssueType.Name = "ddlIssueType";
            this.ddlIssueType.Size = new System.Drawing.Size(529, 21);
            this.ddlIssueType.TabIndex = 9;
            // 
            // ddlAssignee
            // 
            this.ddlAssignee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlAssignee.FormattingEnabled = true;
            this.ddlAssignee.Location = new System.Drawing.Point(89, 93);
            this.ddlAssignee.Name = "ddlAssignee";
            this.ddlAssignee.Size = new System.Drawing.Size(258, 21);
            this.ddlAssignee.TabIndex = 10;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(12, 229);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(63, 13);
            this.lblDescription.TabIndex = 11;
            this.lblDescription.Text = "Description:";
            // 
            // txtSummary
            // 
            this.txtSummary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSummary.Location = new System.Drawing.Point(89, 222);
            this.txtSummary.Name = "txtSummary";
            this.txtSummary.Size = new System.Drawing.Size(529, 20);
            this.txtSummary.TabIndex = 12;
            this.txtSummary.Enter += new System.EventHandler(this.textBox_Focus);
            // 
            // txtDescription
            // 
            this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescription.Location = new System.Drawing.Point(15, 248);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(603, 210);
            this.txtDescription.TabIndex = 13;
            // 
            // btnCreate
            // 
            this.btnCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreate.Location = new System.Drawing.Point(519, 467);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(99, 23);
            this.btnCreate.TabIndex = 14;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // lblComponents
            // 
            this.lblComponents.AutoSize = true;
            this.lblComponents.Location = new System.Drawing.Point(12, 128);
            this.lblComponents.Name = "lblComponents";
            this.lblComponents.Size = new System.Drawing.Size(69, 13);
            this.lblComponents.TabIndex = 15;
            this.lblComponents.Text = "Components:";
            // 
            // componentsPanel
            // 
            this.componentsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.componentsPanel.AutoScroll = true;
            this.componentsPanel.Location = new System.Drawing.Point(89, 128);
            this.componentsPanel.Margin = new System.Windows.Forms.Padding(0);
            this.componentsPanel.Name = "componentsPanel";
            this.componentsPanel.Size = new System.Drawing.Size(529, 91);
            this.componentsPanel.TabIndex = 16;
            // 
            // ddlPriority
            // 
            this.ddlPriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPriority.FormattingEnabled = true;
            this.ddlPriority.Location = new System.Drawing.Point(431, 93);
            this.ddlPriority.Name = "ddlPriority";
            this.ddlPriority.Size = new System.Drawing.Size(187, 21);
            this.ddlPriority.TabIndex = 18;
            // 
            // lblPriority
            // 
            this.lblPriority.AutoSize = true;
            this.lblPriority.Location = new System.Drawing.Point(362, 96);
            this.lblPriority.Name = "lblPriority";
            this.lblPriority.Size = new System.Drawing.Size(38, 13);
            this.lblPriority.TabIndex = 17;
            this.lblPriority.Text = "Priority";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lnkIssueCreated);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(630, 33);
            this.panel1.TabIndex = 19;
            // 
            // lnkIssueCreated
            // 
            this.lnkIssueCreated.AutoSize = true;
            this.lnkIssueCreated.Location = new System.Drawing.Point(12, 9);
            this.lnkIssueCreated.Name = "lnkIssueCreated";
            this.lnkIssueCreated.Size = new System.Drawing.Size(0, 13);
            this.lnkIssueCreated.TabIndex = 0;
            this.lnkIssueCreated.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkIssueCreated_LinkClicked);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 503);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ddlPriority);
            this.Controls.Add(this.lblPriority);
            this.Controls.Add(this.componentsPanel);
            this.Controls.Add(this.lblComponents);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.txtSummary);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.ddlAssignee);
            this.Controls.Add(this.ddlIssueType);
            this.Controls.Add(this.ddlProjects);
            this.Controls.Add(this.lblAssignee);
            this.Controls.Add(this.lblIssueType);
            this.Controls.Add(this.lblProject);
            this.Controls.Add(this.lnkUsername);
            this.Controls.Add(this.lblLoggedInAs);
            this.Name = "MainWindow";
            this.Text = "JIRA Issue Creator";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLoggedInAs;
        private System.Windows.Forms.LinkLabel lnkUsername;
        private System.Windows.Forms.Label lblProject;
        private System.Windows.Forms.Label lblIssueType;
        private System.Windows.Forms.Label lblAssignee;
        private System.Windows.Forms.ComboBox ddlProjects;
        private System.Windows.Forms.ComboBox ddlIssueType;
        private System.Windows.Forms.ComboBox ddlAssignee;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtSummary;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label lblComponents;
        private System.Windows.Forms.FlowLayoutPanel componentsPanel;
        private System.Windows.Forms.ComboBox ddlPriority;
        private System.Windows.Forms.Label lblPriority;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.LinkLabel lnkIssueCreated;

    }
}

