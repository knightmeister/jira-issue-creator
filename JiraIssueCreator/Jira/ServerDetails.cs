﻿/*
 *  JIRA Issue Creator
 *  Copyright (C) 2014 Sam Knight
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

using System;

namespace JiraIssueCreator.Jira
{
    /// <summary>
    /// Storage for information about the JIRA server. 
    /// This class is serialized and saved in the applications directory.
    /// </summary>
    public class ServerDetails
    {
        /// <summary>
        /// Gets or sets the username to log in as.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the encoded credentials for the user.
        /// </summary>
        public string EncodedCredentials { get; set; }

        /// <summary>
        /// Gets or sets the server address.
        /// </summary>
        public string Endpoint { get; set; }

        /// <summary>
        /// Sets the EncodedCredentials property to the encoded representation of the username and password.
        /// </summary>
        /// <param name="username">The users name.</param>
        /// <param name="password">The users password.</param>
        public void SetCredentials(string username, string password)
        {
            // The encoded credentials is in the format of username:password, encoded in Base64.
            string toEncode = string.Format("{0}:{1}", username, password);
            byte[] encodedBytes = System.Text.Encoding.UTF8.GetBytes(toEncode);

            EncodedCredentials = Convert.ToBase64String(encodedBytes);
        }
    }
}
