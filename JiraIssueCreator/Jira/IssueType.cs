﻿/*
 *  JIRA Issue Creator
 *  Copyright (C) 2014 Sam Knight
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace JiraIssueCreator.Jira
{
    /// <summary>
    /// Represents an issue type in Jira.
    /// </summary>
    public class IssueType
    {
        /// <summary>
        /// Gets or sets the issuetype endpoint URL
        /// </summary>
        [JsonProperty("self")]
        public string Endpoint { get; set; }

        /// <summary>
        /// Gets or sets the ID of the issue type.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the issue type description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the issue type name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets whether the issue has subtasks.
        /// </summary>
        public bool Subtask { get; set; }

        /// <summary>
        /// Creates a new instance of the IssueType class.
        /// </summary>
        public IssueType()
        {

        }

        /// <summary>
        /// Overrides the default ToString method.
        /// </summary>
        /// <returns>The Issue Type name.</returns>
        public override string ToString()
        {
            return this.Name;
        }

    }
}
