﻿/*
 *  JIRA Issue Creator
 *  Copyright (C) 2014 Sam Knight
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

using System;
using Newtonsoft.Json;

namespace JiraIssueCreator.Jira
{
    /// <summary>
    /// Defines fields that are posted to create an issue
    /// </summary>
    public class IssueFields
    {
        [JsonProperty("project")]
        public ProjectDto Project { get; set; }

        [JsonProperty("summary")]
        public string Summary { get; set; }

        [JsonProperty("issuetype")]
        public IssueTypeDto IssueType { get; set; }

        [JsonProperty("assignee")]
        public UserDto Assignee { get; set; }

        [JsonProperty("reporter")]
        public UserDto Reporter { get; set; }

        [JsonProperty("priority")]
        public PriorityDto Priority { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("components")]
        public ComponentDto[] Components { get; set; }

        
    }
}
