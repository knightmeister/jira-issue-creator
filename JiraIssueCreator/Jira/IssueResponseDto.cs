﻿/*
 *  JIRA Issue Creator
 *  Copyright (C) 2014 Sam Knight
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

using System;
using Newtonsoft.Json;

namespace JiraIssueCreator.Jira
{
    /// <summary>
    /// Implementation of the Issue creation response DTO from JIRA.
    /// </summary>
    public class IssueResponseDto
    {
        /// <summary>
        /// Gets or sets the Issue ID.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the Key for the issue.
        /// </summary>
        public string Key { get; set; }
        
        /// <summary>
        /// Gets or sets the Endpoint for the issue.
        /// </summary>
        [JsonProperty("self")]
        public string Endpoint { get; set; }
    }
}
