﻿/*
 *  JIRA Issue Creator
 *  Copyright (C) 2014 Sam Knight
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;

namespace JiraIssueCreator.Jira
{
    /// <summary>
    /// Represents an issue in JIRA.
    /// </summary>
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class Issue
    {
        [JsonProperty("fields")]
        public IssueFields Fields { get; set; }

        /// <summary>
        /// Creates a new instance of the Issue class.
        /// </summary>
        public Issue(Project project, string summary, IssueType issueType, User assignee, User reporter, Priority priority, string description, Component[] components)
        {
            Fields = new IssueFields();

            Fields.Project = new ProjectDto(project);
            Fields.Summary = summary;
            Fields.IssueType = new IssueTypeDto(issueType);
            Fields.Assignee = new UserDto(assignee);
            Fields.Reporter = new UserDto(reporter);
            Fields.Priority = new PriorityDto(priority);
            Fields.Description = description;

            // Now convert each of the components to a component dto and set the array
            List<ComponentDto> componentDtos = new List<ComponentDto>();
            foreach (Component component in components)
                componentDtos.Add(new ComponentDto(component));
            Fields.Components = componentDtos.ToArray();
        }

        /// <summary>
        /// Creates the issue on the server.
        /// </summary>
        /// <param name="serverDetails">JIRA server credentials.</param>
        public IssueResponseDto Create(ServerDetails serverDetails)
        {
            // Serialize this class
            string issueJson = JsonConvert.SerializeObject(this);

            // Post it to JIRA
            RestClient client = new RestClient(serverDetails.Endpoint);
            RestRequest request = new RestRequest("rest/api/2/issue", Method.POST);
            request.AddHeader("Authorization", "Basic " + serverDetails.EncodedCredentials);
            request.AddHeader("Accept", "application/json");
            request.AddParameter("application/json", issueJson, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            // Response format
            // {"id":"10321","key":"BUILDER-20","self":"http://jira.svc/rest/api/2/issue/10321"}
            IssueResponseDto responseDto = JsonConvert.DeserializeObject<IssueResponseDto>(response.Content);
            return responseDto;
        }
    }
}
