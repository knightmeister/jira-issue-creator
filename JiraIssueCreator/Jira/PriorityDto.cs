﻿/*
 *  JIRA Issue Creator
 *  Copyright (C) 2014 Sam Knight
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

using System;
using Newtonsoft.Json;

namespace JiraIssueCreator.Jira
{
    /// <summary>
    /// Represents a simple dto for a Priority object.
    /// </summary>
    public class PriorityDto
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Creates a new instance of the PriorityDto object.
        /// </summary>
        public PriorityDto()
        {

        }

        /// <summary>
        /// Creates a new instance of the PriorityDto object.
        /// </summary>
        /// <param name="priority">The source priority.</param>
        public PriorityDto(Priority priority)
        {
            Id = priority.Id;
        }
    }
}
