﻿/*
 *  JIRA Issue Creator
 *  Copyright (C) 2014 Sam Knight
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace JiraIssueCreator.Jira
{
    /// <summary>
    /// Represents a user in JIRA.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Gets or sets the Endpoint for the user.
        /// </summary>
        [JsonProperty("self")]
        public string Endpoint { get; set; }

        /// <summary>
        /// Gets or sets the email address for the user.
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the users name.
        /// </summary>
        [JsonProperty("name")]
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the display name for the user.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Returns the the user details in the format of Display Name (username)
        /// </summary>
        public override string ToString()
        {
            return string.Format("{0} ({1})", DisplayName, Username);
        }
    }
}
