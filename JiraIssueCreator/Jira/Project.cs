﻿/*
 *  JIRA Issue Creator
 *  Copyright (C) 2014 Sam Knight
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

using System;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace JiraIssueCreator.Jira
{
    public class Project
    {
        /// <summary>
        /// Gets or sets the project ID.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the project key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the project REST endpoint URL.
        /// </summary>
        [JsonProperty("self")]
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the project name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the components of the project.
        /// </summary>
        public Component[] Components { get; set; }

        /// <summary>
        /// Gets or sets the issue types for the project.
        /// </summary>
        public IssueType[] IssueTypes { get; set; }

        public User[] AssignableUsers { get; set; }

        /// <summary>
        /// Gets projects from the Server.
        /// </summary>
        /// <param name="serverDetails">JIRA server details.</param>
        /// <returns>An array of JIRA projects.</returns>
        public static Project[] GetProjects(ServerDetails serverDetails)
        {
            RestClient client = new RestClient(serverDetails.Endpoint);
            RestRequest request = new RestRequest("rest/api/2/project", Method.GET);
            request.AddHeader("Authorization", "Basic " + serverDetails.EncodedCredentials);

            // Get the response and cast to the project array and return it.
            IRestResponse response = client.Execute(request);
            Project[] projects = JsonConvert.DeserializeObject<Project[]>(response.Content);

            return projects;
        }

        /// <summary>
        /// Populates assignable users, components and issue types for the project.
        /// </summary>
        public void Populate(ServerDetails serverDetails)
        {
            // The one REST api call will return both issue types and components.
            RestClient client = new RestClient(serverDetails.Endpoint);
            RestRequest request = new RestRequest("rest/api/2/project/{id}", Method.GET);
            request.AddUrlSegment("id", this.Key);
            request.AddHeader("Authorization", "Basic " + serverDetails.EncodedCredentials);

            // Get the response and cast to the project array and return it.
            IRestResponse response = client.Execute(request);
            JObject jo = JObject.Parse(response.Content);
            
            // Pull out the Components
            JToken components = jo.SelectToken("components");
            this.Components = JsonConvert.DeserializeObject<Component[]>(components.ToString());

            // Pull out the issue types
            JToken issues = jo.SelectToken("issueTypes");
            this.IssueTypes = JsonConvert.DeserializeObject<IssueType[]>(issues.ToString());    
   
            // Now query the available users
            // Set up the next REST request.
            request = new RestRequest("rest/api/2/user/assignable/search");
            request.AddParameter("project", this.Key);
            request.AddHeader("Authorization", "Basic " + serverDetails.EncodedCredentials);
            response = client.Execute(request);
            AssignableUsers = JsonConvert.DeserializeObject<User[]>(response.Content);
        }

        /// <summary>
        /// Overrides the default ToString method.
        /// </summary>
        /// <returns>The project name.</returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}
