﻿/*
 *  JIRA Issue Creator
 *  Copyright (C) 2014 Sam Knight
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;

namespace JiraIssueCreator.Jira
{
    /// <summary>
    /// Encapsulates a JIRA priority
    /// </summary>
    public class Priority
    {
        /// <summary>
        /// Gets or sets the priority endpoint.
        /// </summary>
        [JsonProperty("self")]
        public string Endpoint { get; set; }

        /// <summary>
        /// Gets or sets the priority name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the priority description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the priority ID.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets priorities from the server.
        /// </summary>
        /// <param name="serverDetails">JIRA server details.</param>
        /// <returns></returns>
        public static Priority[] GetPriorities(ServerDetails serverDetails)
        {
            RestClient client = new RestClient(serverDetails.Endpoint);
            RestRequest request = new RestRequest("rest/api/2/priority", Method.GET);
            request.AddHeader("Authorization", "Basic " + serverDetails.EncodedCredentials);

            // Get the response and cast to the project array and return it.
            IRestResponse response = client.Execute(request);
            return JsonConvert.DeserializeObject<Priority[]>(response.Content);
        }

        /// <summary>
        /// Overrides the default ToString to return the priority name.
        /// </summary>
        /// <returns>The priority name.</returns>
        public override string ToString()
        {
            return Name;
        }
    }
}
