﻿/*
 *  JIRA Issue Creator
 *  Copyright (C) 2014 Sam Knight
 * 
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using JiraIssueCreator.Jira;
using Resources = JiraIssueCreator.JiraIssueCreatorResources;

namespace JiraIssueCreator
{
    /// <summary>
    /// Implementation of the primary window for the JIRA issue creator.
    /// </summary>
    public partial class MainWindow : Form
    {
        /// <summary>
        /// Storage for all created issues
        /// </summary>
        private List<IssueResponseDto> _createdIssues;

        /// <summary>
        /// Storage for JIRA credentials
        /// </summary>
        private ServerDetails _credentials;
        
        /// <summary>
        /// Storage for all projects the user has access to.
        /// </summary>
        private Project[] _projects;

        /// <summary>
        /// Storage for JIRA priorities.
        /// </summary>
        private Priority[] _priorities;

        /// <summary>
        /// Instantiates the main window.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            getJiraCredentials();
            getProjects();
            getPriorities();
            buildUI();

            localize();

            _createdIssues = new List<IssueResponseDto>();
        }

        /// <summary>
        /// Gets all projects from JIRA.
        /// </summary>
        private void getProjects()
        {
            // Get the projects, and then call Populate to fill all issue types and components.
            Project[] projects = Project.GetProjects(_credentials);
            foreach (Project project in projects)
                project.Populate(_credentials);

            _projects = projects;
        }

        /// <summary>
        /// Gets all priorities from JIRA.
        /// </summary>
        private void getPriorities()
        {
            _priorities = Priority.GetPriorities(_credentials);
            
            ddlPriority.Items.Clear();
            foreach (Priority priority in _priorities)
                ddlPriority.Items.Add(priority);

            // Select the middle priority (good bet it's the default one)
            ddlPriority.SelectedIndex = ddlPriority.Items.Count / 2;
        }

        /// <summary>
        /// Builds the user interface.
        /// </summary>
        private void buildUI()
        {
            ddlProjects.Items.Clear();
            foreach (Project project in _projects)
                ddlProjects.Items.Add(project);

            ddlProjects.SelectedIndex = 0;
        }

        /// <summary>
        /// Gets saved JIRA credentials.
        /// </summary>
        private void getJiraCredentials()
        {
            _credentials = new ServerDetails();
            
            // Comment this message box and fill in the following lines
            MessageBox.Show("You need to enter your username and password! Debugger will now break if attached so you can see where :)");
            if (Debugger.IsAttached)
                Debugger.Break();
            
            _credentials.Username = "username";
            _credentials.SetCredentials("username", "password");
            _credentials.Endpoint = "http://yourjiraserver:8080/";
        }

        /// <summary>
        /// Localize the application window.
        /// </summary>
        private void localize()
        {
            // I prefer to manually bind localizable resources as I find the generated RESX files messy.
            this.Text = Resources.MainWindow_Text;
            lblLoggedInAs.Text = Resources.MainWindow_lblLoggedInAs;
            lnkUsername.Text = _credentials.Username;
            lnkUsername.Left = lblLoggedInAs.Right + 15;

            btnCreate.Text = Resources.MainWindow_btnCreate;
            lblAssignee.Text = Resources.MainWindow_lblAssignee;
            lblComponents.Text = Resources.MainWindow_lblComponents;
            lblIssueType.Text = Resources.MainWindow_lblIssueType;
            lblProject.Text = Resources.MainWindow_lblProject;
            lblPriority.Text = Resources.MainWindow_lblPriority;
        }

        /// <summary>
        /// Gets checked components.
        /// </summary>
        /// <returns>An array of the components that are checked.</returns>
        private Component[] getCheckedComponents()
        {
            // Loop through the items in componentsPanel. If the item is a checkbox check the Tag for a component object.
            List<Component> components = new List<Component>();
            for (int i = 0; i < componentsPanel.Controls.Count; i++)
            {
                CheckBox item = componentsPanel.Controls[i] as CheckBox;
                if (item == null || !item.Checked)
                    continue;

                Component component = item.Tag as Component;
                if (component == null)
                    continue;

                components.Add(component);
            }

            // Return the array of checked compoonents
            return components.ToArray();
        }

        #region Events
        /// <summary>
        /// Gets the project that is selected in the user interface.
        /// </summary>
        /// <returns>The selected project</returns>
        private Project getSelectedProject()
        {
            // Get the selected project
            return _projects[ddlProjects.SelectedIndex];
        }

        /// <summary>
        /// Fired when a new project is selected.
        /// </summary>
        private void ddlProjects_SelectedValueChanged(object sender, EventArgs e)
        {
            // Clear out existing form elements
            ddlIssueType.Items.Clear();
            ddlAssignee.Items.Clear();
            componentsPanel.Controls.Clear();

            // Get the selected project.
            Project project = getSelectedProject();

            // Add issue types and then components and then users
            foreach (IssueType type in project.IssueTypes)
            {
                if (type.Subtask)
                    continue;               // We aren't supporting adding subtasks

                ddlIssueType.Items.Add(type);
            }
            foreach (Component component in project.Components)
            {
                CheckBox chkComponent = new CheckBox();
                chkComponent.Tag = component;
                chkComponent.Text = component.Name;
                componentsPanel.Controls.Add(chkComponent);
            }
            foreach (User user in project.AssignableUsers)
                ddlAssignee.Items.Add(user);

            // Select the first issue type
            ddlIssueType.SelectedIndex = 0;
            ddlAssignee.SelectedIndex = 0;
        }

        /// <summary>
        /// Fired when Create is clicked.
        /// </summary>
        private void btnCreate_Click(object sender, EventArgs e)
        {
            // Validate
            if(string.IsNullOrWhiteSpace(txtSummary.Text))
            {
                MessageBox.Show(Resources.Message_EnterTitle_Text, Resources.Message_EnterTitle_Title, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtSummary.Focus();
                return;
            }

            // Create the issue
            Project project = getSelectedProject();
            Issue issue = new Issue(project,
                txtSummary.Text,
                project.IssueTypes[ddlIssueType.SelectedIndex],
                project.AssignableUsers[ddlAssignee.SelectedIndex],
                new User() { Username = _credentials.Username },
                _priorities[ddlPriority.SelectedIndex],
                txtDescription.Text,
                getCheckedComponents());
            
            IssueResponseDto response = issue.Create(_credentials);
            _createdIssues.Add(response);
            lnkIssueCreated.Text = string.Format(Resources.MainWindow_IssueCreated, response.Key);
            lnkIssueCreated.Tag = response.Key;
        }

        /// <summary>
        /// Fired when focus enters a text field.
        /// </summary>
        private void textBox_Focus(object sender, EventArgs e)
        {
            // Make sure we have a textbox
            TextBox textBox = sender as TextBox;
            if (textBox == null)
                return;

            textBox.SelectAll();
        }

        /// <summary>
        /// Fired when the Issue Created link is clicked.
        /// </summary>
        private void lnkIssueCreated_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Issue key is in the tag of the link label, build up a url to shellex.
            string openUrl = _credentials.Endpoint;
            if (!openUrl.EndsWith("/"))
                openUrl += "/";
            openUrl += "browse/" + (((LinkLabel)sender).Tag.ToString());

            // Open the issue in the users default web browser
            Process.Start(openUrl);
        }
        #endregion
    }
}
