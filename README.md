JIRA Issue Creator
=========

JIRA Issue Creator (JIC) is a simple program for quickly creating issues in JIRA from the desktop, licensed under GPLv2.

The code presently has no error checking, but shows how to use various JIRA REST endpoints to retrieve projects, issue types and to create issues.

The application is localizable.

Frameworks
----

* RestSharp - http://restsharp.org/
* JSON.Net - http://james.newtonking.com/json

Future plans
----

I have a bunch of future plans for this program, which I'll do as I get around to it. These include:

* Adding a hotkey to bring the window up
* Editing created issues
* Add error handling
* Save the users credentials
* Add threading to improve responsiveness of the User Interface

    